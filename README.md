# Useful stuff

This repository contains information I have found useful for work (or fun).

I will try to keep it as organized as possible, but I'm only human.

- [Useful stuff](#useful-stuff)
  - [pigz: a parallel implementation of gzip](#pigz-a-parallel-implementation-of-gzip)
  - [Installing Python packages from git repository](#installing-python-packages-from-git-repository)

## pigz: a parallel implementation of gzip

This is useful to speed up compression of files. I have used it to make a tar.gz file like so

```shell
tar cf - <paths-to-archive> | pigz -p 16 > archive.tar.gz
```

where `-p 16` sets the number of processes used.

Links:

- http://zlib.net/pigz/
- https://github.com/madler/pigz

**Note:** as of today (27 June 2023), the only way I could compile from source was using the `develop` branch.

Sources:
- https://stackoverflow.com/a/39904353

## Installing Python packages from git repository

You can do

```shell
pip install git+https://github.com/<user>/<repository-name>.git
```

or

```shell
pip install --upgrade https://github.com/<user>/<repository-name>.git
```

To specify a branch, you can add `@<branch-name>` after the link (that should not have a trailing slash).

If the repository is private and you are using a ssh key, you can use

```shell
pip install git+ssh://git@github.com/<user>/<repository-name>.git
```

You can find more about VCS support in the docs: https://pip.pypa.io/en/stable/topics/vcs-support/.

Sources:
- https://stackoverflow.com/questions/20101834/pip-install-from-git-repo-branch
- https://stackoverflow.com/questions/15268953/how-to-install-python-package-from-github
